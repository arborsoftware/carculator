import { Calculator } from './Calculator';

describe('Calculator', () => {
  describe('Sales Tax', () => {
    it('should calculate sales tax accurately', () => {
      expect(
        new Calculator({ purchasePrice: 2000, salesTaxPercentage: 7 }).calculateSalesTaxTotal()
      ).toBe(140);
    });

    it('should calculate floating point sales tax accurately', () => {
      expect(
        new Calculator({
          purchasePrice: 25221.01,
          salesTaxPercentage: 7.0753,
        }).calculateSalesTaxTotal()
      ).toBe(1784.46);

      expect(
        new Calculator({
          purchasePrice: 15000.51,
          salesTaxPercentage: 9.12,
        }).calculateSalesTaxTotal()
      ).toBe(1368.05);
    });
  });

  describe('Down Payment', () => {
    it('should calculate with a 20% down payment by default', () => {
      expect(new Calculator({ purchasePrice: 2000 }).calculateDownPayment()).toBe(400);
      expect(new Calculator({ purchasePrice: 20000 }).calculateDownPayment()).toBe(4000);
      expect(new Calculator({ purchasePrice: 15000 }).calculateDownPayment()).toBe(3000);
      expect(new Calculator({ purchasePrice: 25000.52 }).calculateDownPayment()).toBe(5000.1);
      expect(new Calculator({ purchasePrice: 25000.73 }).calculateDownPayment()).toBe(5000.15);
    });
  });

  describe('Monthly Payment', () => {
    it('should accurately calculate a simple monthly payment', () => {
      expect(
        new Calculator({ purchasePrice: 20000, annualInterestRate: 3.15 }).calculateMonthlyPayment()
      ).toBe(360.44);
    });
  });

  describe('Monthly Interest Rate', () => {});
});
