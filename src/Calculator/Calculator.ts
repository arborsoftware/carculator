import math from 'mathjs';

export interface ICalculatorConstructorParameters {
  purchasePrice?: number;
  salesTaxPercentage?: number;
  annualInterestRate?: number;
  downPayment?: number;
  loanTerm?: number;
  monthlyExpenseCost?: number;
  tradeInValue?: number;
  rollTaxIntoPayment?: boolean;
}

export class Calculator {
  public readonly purchasePrice: number = 0;
  public readonly salesTaxPercentage: number = 0;
  public readonly annualInterestRate: number = 0;
  public readonly loanTerm: number = 0;
  public readonly monthlyExpenseCost: number = 0;
  public readonly tradeInValue: number = 0;
  public readonly rollTaxIntoPayment: boolean = false;
  public downPayment: number = 0;

  constructor({
    purchasePrice,
    salesTaxPercentage,
    annualInterestRate,
    downPayment,
    loanTerm,
    monthlyExpenseCost,
    tradeInValue,
    rollTaxIntoPayment,
  }: ICalculatorConstructorParameters) {
    this.purchasePrice = purchasePrice || 0;
    this.salesTaxPercentage = salesTaxPercentage || 0;
    this.annualInterestRate = annualInterestRate || 0;
    this.downPayment = downPayment || 0;
    this.loanTerm = loanTerm || 60;
    this.monthlyExpenseCost = monthlyExpenseCost || 0;
    this.tradeInValue = tradeInValue || 0;
    this.rollTaxIntoPayment = rollTaxIntoPayment || false;
  }

  public calculateSalesTaxTotal(): number {
    // Sales Tax Total is (PurchasePrice * the decimal conversion of sales tax percentage)
    return this.cleanJSNumericalInaccuracies(
      math.multiply(this.purchasePrice, math.divide(this.salesTaxPercentage, 100))
    );
  }

  public calculateCashDown(): number {
    return this.cleanJSNumericalInaccuracies(math.add(
      this.rollTaxIntoPayment ? 0 : this.calculateSalesTaxTotal(),
      this.downPayment
    ) as number);
  }

  public calculateDownPayment(): number {
    return this.cleanJSNumericalInaccuracies(math.multiply(this.purchasePrice, 0.2));
  }

  public calculateMonthlyPayment(): number {
    let loanAmount: number = this.purchasePrice;
    const amountOffPrice = math.add(this.downPayment, this.tradeInValue);
    if (this.rollTaxIntoPayment) {
      loanAmount = math.add(this.purchasePrice, this.calculateSalesTaxTotal()) as number;
    }

    const amountToBorrow = math.subtract(loanAmount, amountOffPrice);
    const monthlyInterestRateDecimal = math.divide(this.calculateMonthlyInterestRate(), 100);

    const amountOwned = math.multiply(
      amountToBorrow,
      math.divide(
        math.multiply(
          monthlyInterestRateDecimal,
          math.pow(math.add(1, monthlyInterestRateDecimal), this.loanTerm)
        ) as number,
        math.subtract(math.pow(math.add(1, monthlyInterestRateDecimal), this.loanTerm), 1) as number
      )
    );

    return this.cleanJSNumericalInaccuracies(math.add(
      amountOwned,
      this.monthlyExpenseCost
    ) as number);
  }

  public calculateMonthlyInterestRate(): number {
    return this.cleanJSNumericalInaccuracies(math.divide(this.annualInterestRate, 12));
  }

  private cleanJSNumericalInaccuracies(
    value: number,
    precision: number = 3,
    decimalsToPreserve: number = 2
  ): number {
    return math.round(
      math.number(math.format(value, { precision, notation: 'fixed' })) as number,
      decimalsToPreserve
    ) as number;
  }
}
